const http = require("http");
const hostname = '127.0.0.1';
const port = 80;

const server = http.createServer((req, res) => {
  const url = req.url;

  res.statusCode = 200;
  res.setHeader('Content-Type', 'text/plain');

  switch (url) {
    case '/RDRSE/key.txt':
      res.end('FD97F125D0D24450E8155104A74EF9FA646F2E964B12932488BF087C8E4D35BF')
      break;
    case '/RDRSE/version.txt':
      res.end('0.0.7.0')
      break;
    default:
      res.end('Hey cutie ;)')
  }

  res.end('Hey cutie ;)');
});

server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});